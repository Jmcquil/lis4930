

# LIS 4930


## Justin McQuillen

### Project 2 # Requirements:

*Includes:*

1. Include splash screen (optional 10 additional pts.).
2. Insert at least five sample tasks (see p. 413, and screenshot below).
3. Test database class (see pp. 424, 425)
4. Must add background color(s) or theme
5. Create and display launcher icon image.

#### README.md file should include the following items:

* Coursetitle, your name, assignment requirements, as per A1;
* Screenshot of running application’s Task List (optional splash screen: 10 additional pts.);


#### Project 2 Screenshots:

| Task List Activity | 
| --- |
| ![Task List Activity](img/first.JPG "Task List Activity") | 













