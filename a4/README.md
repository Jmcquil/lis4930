

# LIS 4368


## Justin McQuillen

### Assignment 4 # Requirements:

*Includes:*

1. Include splash screen image, app title, intro text.
2. Include appropriat eimages.
3. Must use persistent data: Shared Preferences 
4. Widgets and images must be vertically and horizontally aligned.
5. Must add background color(s) or theme
6. Create and displaylauncher icon image

#### README.md file should include the following items:

* Coursetitle, your name, assignment requirements, as per A1;
* Screenshot of running application’s splash screen;
* Screenshot of running application’s invalid screen (with appropriate image);
* Screenshots of running application’s valid screen (with appropriate image);


#### Assignment 4 Screenshots:

|*Splash Screen*|Failed Validation:|Passed Validation:|
|-----------------|------------------|------------------|
|![Splash Page Screenshot](img/splash.JPG)|![Failed Screenshot](img/failed.JPG)|![success Screenshot](img/correct.JPG)












