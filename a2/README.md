

# LIS 4930


## Justin McQuillen

### Assignment 2 # Requirements:

*Includes:*

1. Drop-down menu for total number of guests (including yourself): 1 –10
2. Drop-down menu for tip percentage (5% increments): 0 –25
3. Must add background color(s) or theme
4. Extra Credit (10 pts): Create and displaylauncher icon imag

#### README.md file should include the following items:

* Coursetitle, your name, assignment requirements, as per A1;
* Screenshot of running application’s unpopulateduser interface;
* Screenshot of running application’spopulateduser interface;


#### Assignment 2 Screenshots:

*Screenshot of first interface*:

![First Interface](img/Capture1.JPG)

*Screenshot of second interface:

![Second Interface](img/Capture2.JPG)


