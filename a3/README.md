

# LIS 4930


## Justin McQuillen

### Assignment 3 # Requirements:

*Includes:*

1. Field to enter U.S. dollar amount: 1–100,000
2. Must include toast notification if user enters out-of-range values
3. Radio buttons to convert from U.S. to Euros, Pesos and Canadian currency (must be vertically and horizontally aligned)
4. Must include correct sign for euros, pesos, and Canadian dollars
5. Must add background color(s) or theme6.Create and displaylauncher icon image

#### README.md file should include the following items:

* Coursetitle, your name, assignment requirements, as per A1
* Screenshotofrunning application’s unpopulateduser interface
* Screenshotofrunning application’s toast notification
* Screenshotofrunning application’sconverted currencyuser interface



#### Assignment 3 Screenshots:

*Screenshot of first interface*:

![First Interface](img/img1.JPG)

*Screenshot of second interface:

![Second Interface](img/img2.JPG)

*Screenshot of third interface:

![Third Interface](img/img3.JPG)


