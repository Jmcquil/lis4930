
# LIS 4930 Requirements:

*Course work links:*

1. [A1 README.md](a1/README.md "A1 README.md")

    * Install JDK
    * Install Android Studio
    * Provide screenshots of installation
    * Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)
    * Provide git command descriptions

2. [A2 README.md](a2/README.md "A2 README.md")

    * Drop-down menu for total number of guests (including yourself): 1 –10
    * Drop-down menu for tip percentage (5% increments): 0 –25
    * Must add background color(s) or theme
    * Extra Credit (10 pts): Create and displaylauncher icon imag

3. [A3 README.md](a3/README.md "A3 README.md")

    * Course title, your name, assignment requirements, as per A1
    * Screenshot of running application’s unpopulated user interface
    * Screenshot of running application’s toast notification
    * Screenshot of running application’s converted currencyuser interface

4. [A4 README.md](a4/README.md "A4 README.md")
    * Include splash screen image, app title, intro text.
    * Include appropriat eimages.
    * Must use persistent data: Shared Preferences 
    * Widgets and images must be vertically and horizontally aligned.
    * Must add background color(s) or theme
    * Create and displaylauncher icon image

5. [A5 README.md](a5/README.md "A5 README.md")
    * Coursetitle, your name, assignment requirements, as per A1;
    * Screenshot of running application’s splash screen(list of articles –activity_items.xml);
    * Screenshot of running application’s individual article(activity_item.xml);
    * Screenshots of running application’sdefault browser(article link);

6. [P1 README.md](p1/README.md "P1 README.md")

    * Include splash screen image, app title, intro text.
    * Include artists’ images and media.
    * Images and buttons must be vertically and horizontally aligned.
    * Must add background color(s) or theme
    * Create and displaylauncher icon image

7. [P2 README.md](p2/README.md "P2 README.md")

    * Include splash screen (optional10 additional pts.).
    * Insert at least five sample tasks (see p. 413, and screenshot below).
    * Test database class (see pp. 424, 425)
    * Must add background color(s) or theme
    * Create and display launcher icon image.

   

