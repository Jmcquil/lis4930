> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>


# LIS 4930


## Justin McQuillen

### Assignment 1 # Requirements:

*Four parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chs 1-2)
4. Bitbucket repo links

#### README.md file should include the following items:

* Screenshot of Java Hello
* Screenshot of My First App
* Screenshot of Contacts App
* Git commands

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init-- initializes git repository
2. git status-- shows status of files in the index
3. git add-- add file chnaes to working directory
4. git commit-- commits any files added
5. git push-- sends changes to master branch
6. git pull-- fetch and merge changes
7. git branch-- lists existing branches 

#### Assignment Screenshots:

*Screenshot of running java Hello*:

![Hello Screenshot](img/Hello.JPG)

*Screenshot of My First App:

![FirstApp Screenshot](img/FirstApp.JPG)

*Screenshot of Contacts App 1:

![Contacts 1 Screenshot](img/Contacts1.JPG)

*Screenshot of Contacts App 2:

![Contacts 2 Screenshot](img/Contacts2.JPG)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Jmcquil/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/Jmcquil/myteamquotes/ "My Team Quotes Tutorial")