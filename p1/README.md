

# LIS 4930


## Justin McQuillen

### Project 1 # Requirements:

*Includes:*

1. Include splash screen image, app title, intro text.
2. Include artists’ images and media.
3. Images and buttons must be vertically and horizontally aligned.
4. Must add background color(s) or theme5.Create and displaylauncher icon image

#### README.md file should include the following items:

* Coursetitle, your name, assignment requirements, as per A1;
* Screenshot of running application’s splash screen;
* Screenshot of running application’s follow-up screen (with images and buttons);
* Screenshots of running application’s play and pause user interfaces (with images and buttons);


#### Project 1 Screenshots:

| Splash screen | Follow up screen | Pause and Play |
| --- | --- | --- |
| ![P1 Splash](img/splash.JPG "Splash screen for lis 4930") | ![P1 Follow up](img/first.JPG "Follow up screen") | ![P1 Pause](img/second.JPG "Pause and Play")|














